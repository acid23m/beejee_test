<?php declare(strict_types=1);

\defined('PROJECT_DIR') or \define('PROJECT_DIR', dirname(__DIR__));
require PROJECT_DIR . '/vendor/autoload.php';

$app = new \App\Kernel;
$app->boot();
