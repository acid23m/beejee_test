<?php
declare(strict_types=1);

namespace App;

use Aura\Router\RouterContainer;
use Dotenv\Dotenv;
use Laminas\Diactoros\ServerRequestFactory;
use Psr\Http\Message\ResponseInterface;
use Whoops\Handler\PrettyPageHandler;
use Whoops\Run;

/**
 * Kernel.
 *
 * @package App
 */
final class Kernel
{
    /**
     * Application loader.
     */
    public function boot(): void
    {
        ini_set('session.auto_start', '1');

        // env settings
        $dotenv = Dotenv::createImmutable(PROJECT_DIR);
        $dotenv->load();

        // show errors
        if (getenv('APP_ENV') !== 'production') {
            $whoops = new Run;
            $whoops->pushHandler(new PrettyPageHandler);
            $whoops->register();
        }

        $this->prepareDb();
        $this->prepareHttp();
    }

    private function prepareDb(): void
    {
        $db_file = PROJECT_DIR . '/storage/data.sqlite';
        if (!file_exists($db_file)) {
            $db = new \SQLite3($db_file);

            $db->exec(<<<'SQL'
CREATE TABLE "tasks" (
    "id" INTEGER PRIMARY KEY NOT NULL,
    "name" VARCHAR(50) NOT NULL,
    "email" VARCHAR(50) NOT NULL,
    "description" TEXT NOT NULL,
    "status" VARCHAR(50) NOT NULL,
    "edited_by_admin" INTEGER NOT NULL DEFAULT (0),
    "created_at" DATETIME,
    "updated_at" DATETIME
);
SQL
            );
        }
    }

    private function prepareHttp(): void
    {
        $request = ServerRequestFactory::fromGlobals();

        // router
        $routerContainer = new RouterContainer();
        $map = $routerContainer->getMap();

        require PROJECT_DIR . '/routes/web.php';

        $matcher = $routerContainer->getMatcher();
        $route = $matcher->match($request);

        if (!$route) {
            echo 'No route found for the request.';
            exit;
        }

        foreach ($route->attributes as $key => $val) {
            $request = $request->withAttribute($key, $val);
        }

        $callable = $route->handler;
        /** @var ResponseInterface $response */
        $response = $callable($request);

        foreach ($response->getHeaders() as $name => $values) {
            foreach ($values as $value) {
                header(sprintf('%s: %s', $name, $value), false);
            }
        }
        http_response_code($response->getStatusCode());
        echo $response->getBody();
    }

}
