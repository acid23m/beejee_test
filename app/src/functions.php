<?php declare(strict_types=1);

use Aura\Router\RouterContainer;

if (!function_exists('html_encode')) {
    /**
     * Escapes string.
     *
     * @param string $data
     * @return false|string
     */
    function html_encode(string $data): string
    {
        return htmlspecialchars($data, ENT_QUOTES | ENT_SUBSTITUTE);
    }
}

if (!function_exists('url')) {
    /**
     * Generates link.
     *
     * @param string $route_name
     * @param array $route_params
     * @return string
     * @throws \Aura\Router\Exception\RouteNotFound
     */
    function url(string $route_name, array $route_params = []): string
    {
        $routerContainer = new RouterContainer();
        $map = $routerContainer->getMap();

        require PROJECT_DIR . '/routes/web.php';

        $generator = $routerContainer->getGenerator();

        return $generator->generate($route_name, $route_params);
    }
}

if (!function_exists('session_set')) {
    /**
     * Adds data to session.
     *
     * @param string $key
     * @param $data
     */
    function session_set(string $key, $data): void
    {
        session_start();
        $_SESSION[$key] = serialize($data);
    }
}

if (!function_exists('session_get')) {
    /**
     * Adds data to session.
     *
     * @param string $key
     * @param mixed $default
     * @return mixed
     */
    function session_get(string $key, $default = null)
    {
        session_start();
        if (!isset($_SESSION[$key])) {
            return $default;
        }

        return unserialize($_SESSION[$key]);
    }
}

if (!function_exists('session_has')) {
    /**
     * Checks value existence in session.
     *
     * @param string $key
     * @return bool
     */
    function session_has(string $key): bool
    {
        session_start();
        return isset($_SESSION[$key]);
    }
}

if (!function_exists('session_remove')) {
    /**
     * Removes data from session.
     *
     * @param string $key
     */
    function session_remove(string $key): void
    {
        session_start();
        if (session_has($key)) {
            unset($_SESSION[$key]);
        }
    }
}

if (!function_exists('icon')) {
    /**
     * Renders material icon.
     *
     * @param string $name
     * @param string $style
     * @return string
     * @link https://material.io/resources/icons
     */
    function icon(string $name, string $style = ''): string
    {
        return '<i class="material-icons" style="' . $style . '">' . $name . '</i>';
    }
}
