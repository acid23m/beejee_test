<?php
declare(strict_types=1);

namespace App\Controllers;

use App\Models\Task;
use Laminas\Diactoros\Response\HtmlResponse;
use Laminas\Diactoros\Response\RedirectResponse;
use Pagerfanta\Adapter\ArrayAdapter;
use Pagerfanta\Pagerfanta;
use Pagerfanta\View\TwitterBootstrap4View;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Homepage controller.
 *
 * @package App\Controllers
 */
final class HomeController extends BaseController
{
    /**
     * Displays homepage.
     *
     * @param ServerRequestInterface $request
     * @return HtmlResponse
     */
    public function index(ServerRequestInterface $request): HtmlResponse
    {
        // sorting parameters
        $sorting_params = null;
        if (isset($request->getQueryParams()['sort']) && !empty($request->getQueryParams()['sort'])) {
            $sorting_params = [
                html_encode($request->getQueryParams()['sort']),
                isset($request->getQueryParams()['direction'])
                    ? html_encode($request->getQueryParams()['direction'])
                    : 'ASC',
            ];
        }

        // task records
        $tasks = (new Task)->findAll($sorting_params);

        // pagination
        $paginator = new Pagerfanta(new ArrayAdapter($tasks));
        $paginator->setMaxPerPage(3);
        $paginator->setCurrentPage($request->getQueryParams()['page'] ?? 1);
        $page_links = (new TwitterBootstrap4View)->render($paginator, function (int $page) use ($request) {
            $query_params = $request->getQueryParams();
            $query_params['page'] = $page;
            $query_params_str = http_build_query($query_params);

            return url('home') . (empty($query_params_str) ? '' : "?$query_params_str");
        }, [
            'prev_message' => icon('chevron_left'),
            'next_message' => icon('chevron_right'),
        ]);

        return $this->view('index', [
            'paginated_tasks' => $paginator,
            'status_in_progress' => Task::STATUS__IN_PROGRESS,
            'status_done' => Task::STATUS__DONE,
            'page_links' => $page_links,
        ]);
    }

    /**
     * Saves new task.
     *
     * @param ServerRequestInterface $request
     * @return RedirectResponse
     */
    public function create(ServerRequestInterface $request): RedirectResponse
    {
        $data = $request->getParsedBody();

        // validation
        $errors = [];

        if (empty($data['name']) || empty($data['email']) || empty($data['description'])) {
            $errors[] = 'Все поля обязательны для заполнения.';
        }
        if (!empty($data['email']) && !filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
            $errors[] = "E-mail адрес {$data['email']} указан не верно.";
        }

        // save record
        if (!empty($errors)) {
            session_set('errors', $errors);
        } else {
            $task = new Task([
                'name' => $data['name'],
                'email' => $data['email'],
                'description' => $data['description'],
                'status' => Task::STATUS__IN_PROGRESS,
                'edited_by_admin' => false,
            ]);

            if ($task->create()) {
                session_set('success', ['Новая задача создана.']);
            } else {
                session_set('errors', ['Запись не сохранена.']);
            }
        }

        return new RedirectResponse(url('home'));
    }

    /**
     * Deletes all records.
     *
     * @return RedirectResponse
     */
    public function clear(): RedirectResponse
    {
        if (!$this->isAccessGranted()) {
            return new RedirectResponse(url('home'));
        }

        $result = (new Task)->deleteAll();
        if ($result !== false) {
            session_set('success', ['Все записи удалены.']);
        }

        return new RedirectResponse(url('home'));
    }

    /**
     * Deletes one record.
     *
     * @param ServerRequestInterface $request
     * @return RedirectResponse
     */
    public function delete(ServerRequestInterface $request): RedirectResponse
    {
        if (!$this->isAccessGranted()) {
            return new RedirectResponse(url('home'));
        }

        $id = (int)$request->getAttribute('id');
        $result = (new Task)->delete($id);
        if ($result) {
            session_set('success', ['Запись удалена.']);
        }

        return new RedirectResponse(url('home'));
    }

    /**
     * Displays task to edit.
     *
     * @param ServerRequestInterface $request
     * @return HtmlResponse|RedirectResponse
     */
    public function edit(ServerRequestInterface $request)
    {
        if (!$this->isAccessGranted()) {
            return new RedirectResponse(url('home'));
        }

        $id = (int)$request->getAttribute('id');
        $task = (new Task)->find($id);
        if ($task === null) {
            session_set('errors', ['Запись не найдена.']);

            return new RedirectResponse(url('home'));
        }

        return $this->view('edit', [
            'task' => $task,
            'status_in_progress' => Task::STATUS__IN_PROGRESS,
            'status_done' => Task::STATUS__DONE,
        ]);
    }

    /**
     * Updates task.
     *
     * @param ServerRequestInterface $request
     * @return RedirectResponse
     */
    public function update(ServerRequestInterface $request): RedirectResponse
    {
        if (!$this->isAccessGranted()) {
            return new RedirectResponse(url('home'));
        }

        $id = (int)$request->getAttribute('id');
        $task = (new Task)->find($id);
        if ($task === null) {
            session_set('errors', ['Запись не найдена.']);

            return new RedirectResponse(url('home'));
        }

        $data = $request->getParsedBody();

        // validation
        $errors = [];

        if (empty($data['description'])) {
            $errors[] = 'Текст задачи не может быть пустым.';
        }

        // save record
        if (!empty($errors)) {
            session_set('errors', $errors);
        } else {
            // data to update
            if (!isset($data['status'])) {
                $data['status'] = Task::STATUS__IN_PROGRESS;
            }
            if ($data['description'] !== $task->description) {
                $data['edited_by_admin'] = true;
            }
            $task->setAttributes($data);

            if ($task->update()) {
                session_set('success', ['Запись обновлена.']);
            } else {
                session_set('errors', ['Запись не сохранена.']);
            }
        }

        return new RedirectResponse(url('home'));
    }

}
