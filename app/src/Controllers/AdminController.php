<?php
declare(strict_types=1);

namespace App\Controllers;

use Laminas\Diactoros\Response\HtmlResponse;
use Laminas\Diactoros\Response\RedirectResponse;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Class AdminController.
 *
 * @package App\Controllers
 */
final class AdminController extends BaseController
{
    /**
     * Displays login form.
     *
     * @return HtmlResponse
     */
    public function login(): HtmlResponse
    {
        return $this->view('login');
    }

    /**
     * Authorizes admin.
     *
     * @param ServerRequestInterface $request
     * @return RedirectResponse
     */
    public function auth(ServerRequestInterface $request): RedirectResponse
    {
        $data = $request->getParsedBody();

        // validation
        $errors = [];

        if (empty($data['username']) || empty($data['password'])) {
            $errors[] = 'Все поля обязательны для заполнения.';
        }

        $this->admin->setAttributes($data);
        if (!$this->admin->isCredsValid()) {
            $errors[] = 'Неправильные реквизиты доступа.';
        }

        // authorizes
        if (!empty($errors)) {
            session_set('errors', $errors);

            return new RedirectResponse(url('login'));
        }

        $this->admin->login();
        session_set('success', ['Вход выполнен.']);

        return new RedirectResponse(url('home'));
    }

    /**
     * Signs out.
     *
     * @return RedirectResponse
     * @throws \Aura\Router\Exception\RouteNotFound
     * @throws \Laminas\Diactoros\Exception\InvalidArgumentException
     */
    public function logout(): RedirectResponse
    {
        $this->admin->logout();
        session_set('success', ['Администратор вышел.']);

        return new RedirectResponse(url('home'));
    }

}
