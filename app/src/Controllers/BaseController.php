<?php
declare(strict_types=1);

namespace App\Controllers;

use App\Models\Admin;
use Laminas\Diactoros\Response\HtmlResponse;
use Twig\Environment;
use Twig\Extension\DebugExtension;
use Twig\Loader\FilesystemLoader;
use Twig\TwigFunction;

/**
 * Base controller.
 *
 * @package App\Controllers
 */
class BaseController
{
    /**
     * @var Admin
     */
    protected $admin;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->admin = new Admin;
    }

    /**
     * Renders view.
     *
     * @param string $name
     * @param array $params
     * @return HtmlResponse
     * @throws \Laminas\Diactoros\Exception\InvalidArgumentException
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    protected function view(string $name, array $params = []): HtmlResponse
    {
        $loader = new FilesystemLoader(PROJECT_DIR . '/views');
        $twig = new Environment($loader, [
            'debug' => getenv('APP_ENV') !== 'production',
            'cache' => PROJECT_DIR . '/storage/twig',
        ]);
        $twig->addExtension(new DebugExtension);

        $twig->addFunction(
            new TwigFunction('url', function (string $route_name, array $route_params = []): string {
                return url($route_name, $route_params);
            })
        );
        $twig->addFunction(
            new TwigFunction('errors', function (): ?array {
                return session_get('errors');
            })
        );
        $twig->addFunction(
            new TwigFunction('success', function (): ?array {
                return session_get('success');
            })
        );
        $twig->addFunction(
            new TwigFunction('icon', function (string $name, string $style = ''): string {
                return icon($name, $style);
            })
        );

        $twig->addGlobal('admin', $this->admin);

        $html = $twig->render($name . '.twig', $params);

        // clears massages
        session_remove('errors');
        session_remove('success');

        return new HtmlResponse($html);
    }

    /**
     * Checks access to the action.
     *
     * @return bool
     */
    protected function isAccessGranted(): bool
    {
        if ($this->admin->isLogedIn()) {
            return true;
        }

        session_set('errors', ['Только Администратор может выполнить данное действие.']);

        return false;
    }

}
