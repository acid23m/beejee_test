<?php
declare(strict_types=1);

namespace App\Models;

/**
 * Model.
 *
 * @package App\Models
 */
abstract class AbstractModel
{
    /**
     * Constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        if (!empty($attributes)) {
            $this->setAttributes($attributes);
        }
    }

    /**
     * Gets attribute.
     *
     * @param string $name
     * @return mixed
     * @throws \DomainException
     */
    public function __get(string $name)
    {
        $getter = 'get' . str_replace('_', '', ucwords($name, '_'));
        if (method_exists($this, $getter)) {
            return $this->$getter();
        }

        throw new \DomainException('Getting unknown property: ' . get_class($this) . '::' . $name);
    }

    /**
     * Fills model attributes.
     *
     * @param array $attributes
     */
    abstract public function setAttributes(array $attributes): void;

}
