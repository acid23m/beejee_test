<?php
declare(strict_types=1);

namespace App\Models;

/**
 * Administrator.
 *
 * @package App\Models
 */
final class Admin extends AbstractModel
{
    public const SESSION_ID = 'admin';

    /**
     * @var string
     */
    private $username;
    /**
     * @var string
     */
    private $password;

    /**
     * @inheritDoc
     */
    public function setAttributes(array $attributes): void
    {
        foreach ($attributes as $attribute => $value) {
            try {
                $this->$attribute = $value;
            } catch (\Exception $e) {
            }
        }
    }

    /**
     * Checks admin credentials.
     *
     * @return bool
     */
    public function isCredsValid(): bool
    {
        return $this->username === getenv('ADMIN_LOGIN') && $this->password === getenv('ADMIN_PASSWORD');
    }

    /**
     * Authorizes admin.
     */
    public function login(): void
    {
        session_set(self::SESSION_ID, md5($this->username . $this->password));
    }

    /**
     * Checks admin authorization.
     *
     * @return bool
     */
    public function isLogedIn(): bool
    {
        return session_has(self::SESSION_ID);
    }

    /**
     * Signs out.
     */
    public function logout(): void
    {
        session_remove(self::SESSION_ID);
    }

}
