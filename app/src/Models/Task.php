<?php
declare(strict_types=1);

namespace App\Models;

/**
 * Task.
 *
 * @package App\Models
 */
final class Task extends AbstractModel
{
    public const STATUS__IN_PROGRESS = 'in progress';
    public const STATUS__DONE = 'done';

    private const TABLE_NAME = 'tasks';

    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $email;
    /**
     * @var string
     */
    private $description;
    /**
     * @var string
     */
    private $status;
    /**
     * @var bool
     */
    private $edited_by_admin;
    /**
     * @var \DateTime
     */
    private $created_at;
    /**
     * @var \DateTime
     */
    private $updated_at;

    /**
     * @var \PDO
     */
    private $db;

    /**
     * Task constructor.
     *
     * @param array $attributes
     * @throws \PDOException
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->db = new \PDO('sqlite:' . PROJECT_DIR . '/storage/data.sqlite');
    }

    /**
     * @inheritDoc
     */
    public function setAttributes(array $attributes): void
    {
        if (isset($attributes['id'])) {
            $this->id = (int)$attributes['id'];
        }
        if (isset($attributes['name'])) {
            $this->name = $attributes['name'];
        }
        if (isset($attributes['email'])) {
            $this->email = $attributes['email'];
        }
        if (isset($attributes['description'])) {
            $this->description = $attributes['description'];
        }
        if (
            isset($attributes['status'])
            && in_array($attributes['status'], [self::STATUS__IN_PROGRESS, self::STATUS__DONE], true)
        ) {
            $this->status = $attributes['status'];
        }
        if (isset($attributes['edited_by_admin'])) {
            $this->edited_by_admin = (bool)$attributes['edited_by_admin'];
        }
        if (isset($attributes['created_at'])) {
            if ($attributes['created_at'] instanceof \DateTime) {
                $this->created_at = $attributes['created_at'];
            } elseif (is_string($attributes['created_at']) && !empty($attributes['created_at'])) {
                $this->created_at = \DateTime::createFromFormat('Y-m-d H:i:s', $attributes['created_at'],
                    new \DateTimeZone('UTC'));
            }
        }
        if (isset($attributes['updated_at'])) {
            if ($attributes['updated_at'] instanceof \DateTime) {
                $this->updated_at = $attributes['updated_at'];
            } elseif (is_string($attributes['updated_at']) && !empty($attributes['updated_at'])) {
                $this->updated_at = \DateTime::createFromFormat('Y-m-d H:i:s', $attributes['updated_at'],
                    new \DateTimeZone('UTC'));
            }
        }
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id === null ? null : (int)$this->id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @return bool
     */
    public function isEditedByAdmin(): bool
    {
        return (bool)$this->edited_by_admin;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): ?\DateTime
    {
        if (is_string($this->created_at)) {
            if (empty($this->created_at)) {
                return null;
            }

            return \DateTime::createFromFormat('Y-m-d H:i:s', $this->created_at, new \DateTimeZone('UTC'));
        }

        return $this->created_at;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        if (is_string($this->updated_at)) {
            if (empty($this->updated_at)) {
                return null;
            }

            return \DateTime::createFromFormat('Y-m-d H:i:s', $this->updated_at, new \DateTimeZone('UTC'));
        }

        return $this->updated_at;
    }

    /**
     * Creates new record.
     *
     * @return $this|null
     * @throws \PDOException
     */
    public function create(): ?self
    {
        $query = $this->db->prepare(
            'INSERT INTO ' . self::TABLE_NAME . ' (name, email, description, status, edited_by_admin, created_at, updated_at) VALUES (:name, :email, :description, :status, :edited_by_admin, :created_at, :updated_at)'
        );
        $query->bindParam(':name', $this->name, \PDO::PARAM_STR);
        $query->bindParam(':email', $this->email, \PDO::PARAM_STR);
        $query->bindParam(':description', $this->description, \PDO::PARAM_STR);
        $query->bindParam(':status', $this->status, \PDO::PARAM_STR);

        $edited_by_admin = (int)$this->edited_by_admin;
        $query->bindParam(':edited_by_admin', $edited_by_admin, \PDO::PARAM_INT);

        if ($this->created_at instanceof \DateTime) {
            $created_at = $this->created_at->format('Y-m-d H:i:s');
        } elseif (is_string($this->created_at) && !empty($this->created_at)) {
            $created_at = $this->created_at;
        } else {
            $created_at = (new \DateTime('now', new \DateTimeZone('UTC')))->format('Y-m-d H:i:s');
        }
        $query->bindParam(':created_at', $created_at, \PDO::PARAM_STR);

        if ($this->updated_at instanceof \DateTime) {
            $updated_at = $this->updated_at->format('Y-m-d H:i:s');
        } elseif (is_string($this->updated_at) && !empty($this->updated_at)) {
            $updated_at = $this->updated_at;
        } else {
            $updated_at = (new \DateTime('now', new \DateTimeZone('UTC')))->format('Y-m-d H:i:s');
        }
        $query->bindParam(':updated_at', $updated_at, \PDO::PARAM_STR);

        if (!$query->execute()) {
            return null;
        }

        $this->setAttributes([
            'id' => $this->db->lastInsertId(),
            'created_at' => $created_at,
            'updated_at' => $updated_at,
        ]);

        return $this;
    }

    /**
     * Updates existing record.
     *
     * @return $this|bool
     * @throws \PDOException
     */
    public function update()
    {
        $query = $this->db->prepare(
            'UPDATE ' . self::TABLE_NAME . ' SET name=:name, email=:email, description=:description, status=:status, edited_by_admin=:edited_by_admin, updated_at=:updated_at WHERE id=:id'
        );
        $query->bindParam(':id', $this->id, \PDO::PARAM_INT);
        $query->bindParam(':name', $this->name, \PDO::PARAM_STR);
        $query->bindParam(':email', $this->email, \PDO::PARAM_STR);
        $query->bindParam(':description', $this->description, \PDO::PARAM_STR);
        $query->bindParam(':status', $this->status, \PDO::PARAM_STR);
        $edited_by_admin = (int)$this->edited_by_admin;
        $query->bindParam(':edited_by_admin', $edited_by_admin, \PDO::PARAM_INT);
        $updated_at = (new \DateTime('now', new \DateTimeZone('UTC')))->format('Y-m-d H:i:s');
        $query->bindParam(':updated_at', $updated_at, \PDO::PARAM_STR);

        if (!$query->execute()) {
            return false;
        }

        return $this;
    }

    /**
     * Retrieves one record by its id.
     *
     * @param int $id
     * @return $this|null
     * @throws \PDOException
     */
    public function find(int $id): ?self
    {
        $query = $this->db->prepare('SELECT * FROM ' . self::TABLE_NAME . ' WHERE id=:id');
        $query->bindParam(':id', $id, \PDO::PARAM_INT);
        $query->execute();

        $data = $query->fetch(\PDO::FETCH_ASSOC);

        if ($data === false) {
            return null;
        }

        $this->setAttributes($data);

        return $this;
    }

    /**
     * Retrieves all records.
     *
     * @param array|null $sorting_params
     * @return self[]
     */
    public function findAll(?array $sorting_params = null): array
    {
        $sql = 'SELECT * FROM ' . self::TABLE_NAME;

        if (is_array($sorting_params)) {
            [$sort_attr, $sort_direct] = $sorting_params;
            $sql .= " ORDER BY $sort_attr $sort_direct";
        }

        $query = $this->db->query($sql);

        if ($query === false) {
            return [];
        }

        return $query->fetchAll(\PDO::FETCH_FUNC, function (...$attributes) {
            return new self(
                array_combine(
                    ['id', 'name', 'email', 'description', 'status', 'edited_by_admin', 'created_at', 'updated_at'],
                    $attributes
                )
            );
        });
    }

    /**
     * Truncates table.
     *
     * @return false|int
     */
    public function deleteAll()
    {
        return $this->db->exec('DELETE FROM ' . self::TABLE_NAME);
    }

    /**
     * Deletes record with specified id.
     *
     * @param int $id
     * @return bool
     * @throws \PDOException
     */
    public function delete(int $id): bool
    {
        $query = $this->db->prepare('DELETE FROM ' . self::TABLE_NAME . ' WHERE id=:id');
        $query->bindParam(':id', $id, \PDO::PARAM_INT);

        return $query->execute();
    }

}
