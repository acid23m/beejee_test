<?php declare(strict_types=1);

use App\Controllers\AdminController;
use App\Controllers\HomeController;
use Aura\Router\Map;

/** @var Map $map */

$homeController = new HomeController;

$map->get('home', '/', [$homeController, 'index']);
$map->post('create-task', '/create', [$homeController, 'create']);
$map->get('clear', '/clear', [$homeController, 'clear']);
$map->get('delete-task', '/delete/{id}', [$homeController, 'delete']);
$map->get('edit-task', '/edit/{id}', [$homeController, 'edit']);
$map->post('update-task', '/update/{id}', [$homeController, 'update']);

$adminController = new AdminController;

$map->get('login', '/login', [$adminController, 'login']);
$map->post('auth', '/auth', [$adminController, 'auth']);
$map->get('logout', '/logout', [$adminController, 'logout']);
