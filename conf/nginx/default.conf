server {
  listen 80;

  server_name localhost;
  root /app/public;

  index index.html index.htm index.php;

  location / {
    try_files $uri $uri/ /index.php?$query_string;
  }

  # remove trailing slash to please routing system
  if (!-d $request_filename) {
    rewrite ^/(.+)/$ /$1 permanent;
  }

  location ~ \.php$ {
    fastcgi_param REMOTE_ADDR $http_x_real_ip;
    fastcgi_pass fpm:9000;
    fastcgi_index index.php;
    fastcgi_split_path_info ^(.+\.php)(/.+)$;
    fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
#    fastcgi_param HTTPS on;
#    fastcgi_param PHP_ADMIN_VALUE open_basedir=$base/:/usr/lib/php/:/tmp/;
    fastcgi_intercept_errors off;

    fastcgi_buffer_size 128k;
    fastcgi_buffers	256 16k;
    fastcgi_busy_buffers_size 256k;
    fastcgi_temp_file_write_size 256k;

    include fastcgi_params;
  }

  location ~* ".+\.(?:css|js|eot|otf|woff|woff2|ttf|svg|svgz|bmp|jpe?g|gif|png|webp|ico|mp4|swf|ogg|ogv|mid|midi|wav|mp3|rss|atom|zip|tar|tgz|gz|rar|7z|bz2|pdf|doc|docx|odt|rtf|xls|ppt|exe|bat|sh)$" {
    access_log off;
    log_not_found off;
    expires 24h;
    add_header Cache-Control "public, must-revalidate";
    fastcgi_hide_header "Set-Cookie";
    etag on;
    max_ranges 0;
  }

  location = /favicon.ico {
    allow all;
    access_log off;
    log_not_found off;
  }

  location = /robots.txt {
    allow all;
    access_log off;
    log_not_found off;
  }

  location /sitemap {
    allow all;
    access_log off;
    log_not_found off;
  }

  location ~* /\. {
    deny all;
  }

  location ~ /\.(?!well-known).* {
    deny all;
  }

  error_page 404 /index.php;
}
