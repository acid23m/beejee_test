#
# Project tools.
# Documentation for makefile:
# - https://www.gnu.org/software/make/manual/make.html
# - http://linux.yaroslavl.ru/docs/prog/gnu_make_3-79_russian_manual.html
#
# Usage:
# make - shows help with available commands
# make [command] - runs specific command
#

SHELL=/bin/bash

PROJECT_DIR=${PWD}
APP_DIR=$(PROJECT_DIR)/app
CONF_DIR=$(PROJECT_DIR)/conf
CONTAINER_APP_PATH=/app
HOST_IP=$(shell ip -4 addr show scope global dev docker0 | grep inet | awk '{print $$2}' | cut -d / -f 1)
OS_USER_ID=$(shell id -u)
OS_USER_GROUP=$(shell id -g)


DEFAULT_GOAL := help
.PHONY: help
help:
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m\n"} /^[a-zA-Z0-9_-]+:.*?##/ { printf "  \033[36m%-27s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)


# ensures of existing .env file
$(PROJECT_DIR)/.env: $(PROJECT_DIR)/.env.example
	@if [[ -f $@ ]]; then \
		echo "The $(<F) file has changed. Please check your $(@F) file." ; \
		touch $@; \
		exit 1; \
	else \
		echo "Please configure project by modifing $(@F) file: nano $(@F)" ; \
		cp -a $< $@ ; \
		chmod 640 $@ ; \
		exit 1 ; \
	fi

include $(PROJECT_DIR)/.env

CONTAINER_PHP=${COMPOSE_PROJECT_NAME}_fpm

define SET_PERMISSIONS=
sudo chown -R $(OS_USER_ID):$(OS_USER_GROUP) $(PROJECT_DIR)
sudo find $(PROJECT_DIR) -type d -exec chmod 755 {} \;
sudo find $(PROJECT_DIR) -type f -exec chmod 644 {} \;
sudo find $(APP_DIR)/public -type d -exec chmod 775 {} \; ; \
sudo find $(APP_DIR)/public -type f -exec chmod 664 {} \; ; \
sudo find $(APP_DIR)/storage -type d -exec chmod 777 {} \; ; \
sudo find $(APP_DIR)/storage -type f -exec chmod 666 {} \; ; \
sudo find $(APP_DIR)/vendor -type d -exec chmod 775 {} \; ; \
sudo find $(APP_DIR)/vendor -type f -exec chmod 664 {} \; ; \
sudo find $(APP_DIR)/vendor/bin -type f -exec chmod 774 {} \;
endef

define COMPOSER_DEPS=
if [[ "${APP_ENV}" == "production" ]]; then \
	docker run --rm -i \
		-v $(APP_DIR):$(CONTAINER_APP_PATH) \
		-w $(CONTAINER_APP_PATH) \
		composer update --prefer-dist --no-dev --no-suggest --optimize-autoloader; \
	docker run --rm -i \
		-v $(APP_DIR):$(CONTAINER_APP_PATH) \
		-w $(CONTAINER_APP_PATH) \
		composer dump-autoload --optimize --no-dev --classmap-authoritative; \
else \
	docker run --rm -i \
		-v $(APP_DIR):$(CONTAINER_APP_PATH) \
		-w $(CONTAINER_APP_PATH) \
		composer update --prefer-dist --optimize-autoloader -vvv; \
	docker run --rm -i \
		-v $(APP_DIR):$(CONTAINER_APP_PATH) \
		-w $(CONTAINER_APP_PATH) \
		composer dump-autoload --optimize -vvv; \
fi
endef


##@ Docker containers


$(APP_DIR)/.env: $(APP_DIR)/.env.example
	@cp -np $(APP_DIR)/.env.example $(APP_DIR)/.env


.PHONY: start
start: $(PROJECT_DIR)/.env ## Runs project.
	# runs containers
	@(export DOCKERHOST_IP=$(HOST_IP) USER_ID=$(OS_USER_ID) USER_GROUP=$(OS_USER_GROUP) && docker-compose -p ${COMPOSE_PROJECT_NAME} up -d --build --remove-orphans --no-recreate)


.PHONY: stop
stop: $(PROJECT_DIR)/.env ## Stops project.
	# removes containers
	-@docker-compose down --remove-orphans


.PHONY: restart
restart: ## Restarts project.
	-@$(MAKE) stop
	@$(MAKE) start


.PHONY: ssh
ssh: ## Connects to the container with application as root.
	@docker exec -it \
		-w $(CONTAINER_APP_PATH) \
		$(CONTAINER_PHP) \
		bash


##@ Application


$(APP_DIR)/vendor: $(APP_DIR)/composer.json
	@$(COMPOSER_DEPS)

.PHONY: composer
composer: ## Installs/upgrades backend dependencies.
	@$(COMPOSER_DEPS)
	@$(SET_PERMISSIONS)


##@ Tools


.PHONY: perm
perm: ## Sets permissions for project.
	@$(SET_PERMISSIONS)
